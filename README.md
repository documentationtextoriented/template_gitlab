Provides libs and pipelines for image/document creation from text format (graphviz, markdown, mermaid).

Coming soon : asciidoc, plantuml

## Basics

### with libs
The *libs* directory contains functions which can be used with [gitlab job extension](https://docs.gitlab.com/ce/ci/yaml/README.html#extends) in your project pipelines.

```mermaid

flowchart LR

subgraph YourProjectUsingLibs
 job
 yaml[(.gitlab-ci.yml)]
 yaml--define-->job
 out[(output directory)]
end

subgraph documentationtextoriented/template_gitlab/

  subgraph libs/a2b.yml
    function[.a2b]
    function--generates files in-->out
   end

end

yaml -- remote include --> libs/a2b.yml
job -- extends entry name --> function


```

### with pipelines
The *pipelines* directory contains definition of takeaway pipelines.

```mermaid

flowchart LR

subgraph YourProjectUsingPipelines
  yaml2[(.gitlab-ci.yml)]
end

subgraph documentationtextoriented/template_gitlab/

  subgraph pipelines/a2b.yml
    job2[job_a2b]
  end

end
yaml2 -- remote include --> pipelines/a2b.yml
YourProjectUsingPipelines --has-->job2
```


## Libs

| yaml to include   | entry name    |  params                     | output directory | examples    |
|-------------------|---------------|-----------------------------|------------------|-------------|
|  [libs/dot2xxx.yml](https://gitlab.com/documentationtextoriented/template_gitlab/raw/master/libs/dot2xxx.yml) | .dot2xxx      | SOURCES, OUTPUT_FORMAT    | public/ | [example project](https://gitlab.com/documentationtextoriented/graphviz_using_libs)        |
| [libs/md2pdf.yml](https://gitlab.com/documentationtextoriented/template_gitlab/-/raw/master/libs/md2pdf.yml)  | .markdown2html, .html2pdf | SOURCES, PANDOC_CSS_STYLE|public/ |              |
| [libs/mermaid2xxx.yml](https://gitlab.com/documentationtextoriented/template_gitlab/-/raw/master/libs/mermaid2xxx.yml) | .mmd2xxx | SOURCES, OUTPUT_FORMAT | public/ | [example project](https://gitlab.com/documentationtextoriented/mermaid_using_libs) |


## Pipelines

| yaml to include   | params (*optionnal*)       | examples    |
|-------------------|----------------------------|-------------|
| [pipelines/dot2xxx.yml](https://gitlab.com/documentationtextoriented/template_gitlab/-/raw/master/pipelines/dot2xxx.yml) | SOURCES | [example project](https://gitlab.com/documentationtextoriented/graphviz_using_pipelines) |
| [pipelines/md2pdf.yml](https://gitlab.com/documentationtextoriented/template_gitlab/-/raw/master/pipelines/md2pdf.yml) | SOURCES, *PANDOC_CSS_STYLE* | [example project](https://gitlab.com/documentationtextoriented/markdown2pdf) |
| [pipelines/mermaid2xxx.yml](https://gitlab.com/documentationtextoriented/template_gitlab/-/raw/master/pipelines/mermaid2xxx.yml) | SOURCES | [example](https://gitlab.com/documentationtextoriented/mermaid_using_pipelines)


